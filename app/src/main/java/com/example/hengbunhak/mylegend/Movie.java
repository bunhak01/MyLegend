package com.example.hengbunhak.mylegend;

/**
 * Created by pirang on 7/10/17.
 */

public class Movie {

    private String title;
    private String posterUrl;
    private String link;
    private String ShowDate;


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Movie(String title, String posterUrl, String link, String showDate) {
        this.title = title;
        this.posterUrl = posterUrl;
        this.link = link;
        ShowDate = showDate;
    }

    public String getShowDate() {

        return ShowDate;
    }

    public void setShowDate(String showDate) {
        ShowDate = showDate;
    }

    public String getPosterUrl() {
        return posterUrl;
    }

    public void setPosterUrl(String posterUrl) {
        this.posterUrl = posterUrl;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    @Override
    public String toString() {
        return this.title;
    }
}
